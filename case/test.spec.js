import { Selector, ClientFunction } from "testcafe"
import { shippings, payWays } from '../config/'
import { getDate } from '../utils'

require('dotenv').config()
const {
  DSHOP_ACCOUNT,
  DSHOP_PASSWORD,
  DSHOP_HOSTNAME,
  DSHOP_PRODUCT_PATH,
  IS_PROD
} = process.env

fixture`Getting Started`.page`${DSHOP_HOSTNAME}${DSHOP_PRODUCT_PATH}`; // 1. 進入 TestCafe Example Page

const cookieAlert = Selector('.close');
const variant = Selector('.detail-variants-btns > .btn:nth-child(1)');
const buynow = Selector('.btn-buy-now')
const loginBtn = Selector(".card-body")
  .find("button")
  .withText("登入")

const plusButton = Selector(".input-numeric")
  .find("button")
  .withText("+")

const nextBtn = Selector(".form-footer")
  .find("button")
  .withText("下一步")

const getPageUrl = ClientFunction(() => window.location.href);
const orderProductNumber = Selector('.text-md-left.quantity > div > span:nth-child(2)')
const countySelect = Selector("select[name='county']")
const districtSelect = Selector("select[name='district']")
const submitBtn = Selector(".form-footer")
  .find("button")
  .withText("送出訂單")

const selectStoreBtn = Selector("#selectStore")
const storeIDby711 = Selector(IS_PROD.toLocaleLowerCase() === 'true' ? "#byID" : "#UnimartPost")
const storeItemby711 = Selector('#ol_stores').find("li")
const sevenDataBtn = Selector('#sevenDataBtn')
const acceptBtnby711 = Selector('#AcceptBtn')
const orderAdress = Selector('#payment-params > div > section:nth-child(3) > div.py-3.p-2 > div:nth-child(2) > div > div > div.col-md.p-0')
const orderListAlertMsg = Selector('div').withAttribute('role', 'alert')

/**
 * 加入購物車
 * @param {Test Controller} t 
 */
const addtoCart = async t => {
  // await t.resizeWindow(900, 600)

  if (await cookieAlert.exists) {
    await t.click(cookieAlert) // 把 cookie 提示關掉
  }

  await t
  .click(variant)
  .doubleClick(buynow)
  .expect(getPageUrl())
  .contains(`${DSHOP_HOSTNAME}/login`)
}

/**
 * 登入頁
 * @param {Test Controller} t
 */
const maybeLogin = async t => {
  await t
    .typeText("#email", DSHOP_ACCOUNT)
    .typeText("#password", DSHOP_PASSWORD)
    .doubleClick(loginBtn)
}

/**
 * 購物車頁
 * @param {Test Controller} t
 */
const submitCart = async t => {
  await t
    .wait(1000)
    .expect(getPageUrl())
    .eql(`${DSHOP_HOSTNAME}/shopping/cart`)
    .click(plusButton)
    .wait(300) // 因為在 publisher 的 function，有加上 debounce 300 ms
    .doubleClick(nextBtn)
    .expect(orderProductNumber.innerText) // 檢查填寫資料頁面的商品數量
    .eql('x ' + 2)
}

/**
 * 選擇寄送方式
 * @param {string} way
 * @param {Test Controller} t
 */
const submitShippingBy = async (way, t) => {
  await t
    .click(Selector(`input[value='${way}']`))
    .typeText("input[name='recipient']", "Roy")
    .typeText("input[name='tel']", "0956331322")

  if (way === 'Normal.Delivery' || way.indexOf('Other.Custom') > -1) {
    await t
      .click(countySelect)
      .click(countySelect.find("option").withText("台中市"))
      .click(districtSelect)
      .click(districtSelect.find("option").withText("北區"))
      .typeText("input[name='address']", "進化路322號3樓")
  } else {
    if (IS_PROD.toLocaleLowerCase() === 'true') {
      await t
        .click(selectStoreBtn)
        .click(storeIDby711)
        .switchToIframe('#frmMain')
        .typeText("input[id='storeIDKey']", "177522")
        .click(Selector('#send'))
        .click(storeItemby711)
        .switchToMainWindow()
        .click(sevenDataBtn)
        .click(acceptBtnby711)
        .click(Selector('#submit_butn'))
    } else {
      await t
        .click(selectStoreBtn)
        .click(storeIDby711)
    }
  }
}

/**
 * 選擇付款方式
 * @param {string} payWay
 * @param {Test Controller} t
 */
const paymentBy = async (payWay, t) => {
  await t
    .click(Selector(`input[id='${payWay}']`))
    .typeText("textarea[name='memo']", "下單測試啦～")
    .click(Selector(`label[for="checkTermsNotice"]`))
    .click(submitBtn)
    .expect(getPageUrl())
    .contains(`${DSHOP_HOSTNAME}/shopping/payment`, '要有 payment')
}

/**
 * 檢查綠界有無串接，與門市或地址是否正確
 * @param {string} payWay
 * @param {boolean} isStore
 * @param {Test Controller} t
 */
const checkOrder = async (payWay, isStore, t) => {
  if (isStore && payWay !== 'Normal.ATM') {
    let storeName
    if (payWay !== 'Normal.ATM') {
      storeName = Selector('#payment-params > div > section:nth-child(3) > div.py-3.p-2 > div:nth-child(2) > div.col-md-6.mb-3.mb-md-2')
    } else {
      storeName = Selector('#payment-params > div > section.font-weight-normal.my-4.py-4  > div.py-3.p-2 > div:nth-child(2) > div.col-md-6.mb-3.mb-md-2')
    }
    await t
      .expect(storeName.innerText)
      .eql(IS_PROD.toLocaleLowerCase() === 'true' ? '門市名稱進合門市' : '門市名稱馥樺門市')
  } else if (payWay === 'Normal.ATM') {
    await t
      .expect(Selector('#payment-params > div > section:nth-child(4) > div.py-3.p-2 > div:nth-child(2) > div > div > div.col-md.p-0').innerText)
      .eql('台中市北區進化路328號')
      .click(Selector('.fa-user'))
      .click(Selector('.dropdown > ul > li:nth-child(2)'))
      .click(Selector('.item-list > a:nth-child(1)'))
      .expect(orderListAlertMsg.innerText)
      .contains('您這筆訂單尚未付款', '沒有未付款字眼')
      .typeText("input[id='date']", getDate())
      .typeText("input[id='lastFiveDigits']", '12345')
      .click(Selector('.payment-btn'))
      .expect(orderListAlertMsg.innerText)
      .contains('已回填匯款資訊', '沒有已回填字眼')
    } else {
    await t
      .expect(orderAdress.innerText)
      .eql('台中市北區進化路322號3樓')
  }
  if (payWay !== 'Normal.COD' && payWay !== 'Normal.ATM') {
    await t
      .click(Selector('.payment-btn'))
      .expect(getPageUrl())
      .contains(IS_PROD.toLocaleLowerCase() === 'true' ? 'https://payment.ecpay.com.tw' : 'https://payment-stage.ecpay.com.tw', '要有 ecpay')
  }
}

payWays.forEach(item => {
  test(`Test 一般宅配 by ${item.name}`, async t => {
    console.log('我在程式裡面，印出環境變數來看: ',  DSHOP_ACCOUNT,
    DSHOP_PASSWORD,
    DSHOP_HOSTNAME,
    DSHOP_PRODUCT_PATH,
    IS_PROD)
    await addtoCart(t)
    await maybeLogin(t)
    await submitCart(t)
    await submitShippingBy(shippings['Delivery'], t)
    await paymentBy(item.selector, t)
    await checkOrder(item.selector, false, t)
  });
})

// payWays.forEach(item => {
//   test(`Test 7-11 by ${item.name}`, async t => {
//     await addtoCart(t)
//     await maybeLogin(t)
//     await submitCart(t)
//     await submitShippingBy(shippings['7ELEVENB2C'], t)
//     await paymentBy(item.selector, t)
//     await checkOrder(item.selector, true, t)
//   })
// })

// payWays.forEach(item => {
//   test(`Test 自訂物流 by ${item.name}`, async t => {
//     await addtoCart(t)
//     await maybeLogin(t)
//     await submitCart(t)
//     await submitShippingBy(shippings['Custom'], t)
//     await paymentBy(item.selector, t)
//     await checkOrder(item.selector, false, t)
//   })
// })
