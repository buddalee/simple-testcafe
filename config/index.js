const browsers = [
  // { name: 'safari', isMobile: false },
  { name: 'chrome', isMobile: false },
  // { name: 'chrome', isMobile: true }
]

const payWays = [
  { name: '實體 ATM', selector: 'ECPay.ATM'},
  // { name: '超商條碼', selector: 'ECPay.Barcode'},
  // { name: '信用卡', selector: 'ECPay.CreditCard'},
  // { name: '超商代碼', selector: 'ECPay.CVS'},
  // { name: '信用卡分期', selector: 'ECPay.Installment'},
  // { name: '網路 ATM', selector: 'ECPay.WebATM'},
  // { name: '貨到付款', selector: 'Normal.COD'},
  // { name: '銀行帳戶匯款', selector: 'Normal.ATM'}
]

const shippings = {
	'Delivery': 'Normal.Delivery',
  '7ELEVENC2C': 'ECPay.UNIMARTC2C',
  '7ELEVENB2C': 'ECPay.UNIMART',
  'Custom': 'Other.Custom1'
}

// testJobs = [
//   shopingbyatm,
//   shopingbywebatm,
//   shopingbywebbarcode,
// ]

module.exports = {
  shippings,
  browsers,
  payWays
}
