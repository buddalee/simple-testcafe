const scrollTo = async (driver, ByElement) => {
  const element = await driver.findElement(ByElement)
  await driver.executeScript(`arguments[0].scrollIntoView();`, element)
}

const asyncForEach = async function (array, callback) {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
}

const getDate = () => {
  const date = new Date()
  const year = date.getFullYear()
  const mm = date.getMonth() + 1
  const dd = date.getDate();
  const hour = date.getHours()
  const min = date.getMinutes()
  return [
    year + '-',
    (mm > 9 ? '' : '0') + mm + '-',
    (dd > 9 ? '' : '0') + dd + ' ',
    hour + ':',
    min
  ].join('')
}

module.exports = {
  scrollTo,
  asyncForEach,
  getDate
}